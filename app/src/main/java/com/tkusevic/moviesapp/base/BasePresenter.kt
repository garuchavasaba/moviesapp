package com.tkusevic.moviesapp.base


interface BasePresenter<in T> {

    fun setBaseview(baseView: T)
}