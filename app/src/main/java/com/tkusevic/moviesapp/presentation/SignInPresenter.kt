package com.tkusevic.moviesapp.presentation

import com.facebook.AccessToken
import com.tkusevic.moviesapp.base.BasePresenter
import com.tkusevic.moviesapp.ui.signIn.SignInView


interface SignInPresenter : BasePresenter<SignInView> {

    fun onSignInClick(email: String, password: String)

    fun handleFacebookAccessToken(token: AccessToken)
}