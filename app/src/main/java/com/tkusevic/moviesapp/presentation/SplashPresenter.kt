package com.tkusevic.moviesapp.presentation

import com.tkusevic.moviesapp.base.BasePresenter
import com.tkusevic.moviesapp.ui.splash.SplashView


interface SplashPresenter: BasePresenter<SplashView> {

    fun checkPrefs()
}