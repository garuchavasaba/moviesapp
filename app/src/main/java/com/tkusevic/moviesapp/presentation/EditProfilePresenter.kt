package com.tkusevic.moviesapp.presentation

import com.tkusevic.moviesapp.base.BasePresenter
import com.tkusevic.moviesapp.ui.edit_profile.EditProfileView


interface EditProfilePresenter : BasePresenter<EditProfileView> {

    fun setCurrentProfile()

    fun saveChanges(aboutMe: String, movieDescription: String, name: String)
}