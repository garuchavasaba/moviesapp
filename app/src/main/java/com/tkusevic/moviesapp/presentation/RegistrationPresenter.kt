package com.tkusevic.moviesapp.presentation

import com.tkusevic.moviesapp.base.BasePresenter
import com.tkusevic.moviesapp.ui.registration.RegistrationView

/**
 * Created by tkusevic on 15.02.2018..
 */
interface RegistrationPresenter : BasePresenter<RegistrationView> {

    fun onRegistrationClick(email: String, password: String, name: String)
}