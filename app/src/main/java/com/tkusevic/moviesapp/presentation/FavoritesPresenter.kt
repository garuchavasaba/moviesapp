package com.tkusevic.moviesapp.presentation

import com.tkusevic.moviesapp.base.BasePresenter
import com.tkusevic.moviesapp.data.model.Movie
import com.tkusevic.moviesapp.ui.movies.views.FavoritesView


interface FavoritesPresenter : BasePresenter<FavoritesView> {

    fun getFavoriteMovies()

    fun onLikeTapped(movie: Movie)
}