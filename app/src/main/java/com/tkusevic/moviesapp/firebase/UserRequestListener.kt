package com.tkusevic.moviesapp.firebase

import com.tkusevic.moviesapp.data.model.User

interface UserRequestListener {

    fun onSuccessfulRequest(user: User)

    fun onFailedRequest()
}