package com.tkusevic.moviesapp.firebase


interface EmptyRequestListener {

    fun onSuccessfulRequest()

    fun onFailedRequest()
}