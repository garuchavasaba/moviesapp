package com.tkusevic.moviesapp.firebase

import com.tkusevic.moviesapp.data.model.Movie
import com.tkusevic.moviesapp.data.model.User


interface MoviesRequestListener {

    fun onSuccessfulRequestMovies(movies: List<Movie>)

    fun onFailedRequestMovies()
}