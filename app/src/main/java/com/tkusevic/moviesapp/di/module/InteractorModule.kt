package com.tkusevic.moviesapp.di.module

import com.tkusevic.moviesapp.interaction.MoviesInteractor
import com.tkusevic.moviesapp.interaction.MoviesInteractorImpl
import dagger.Binds
import dagger.Module


@Module(includes = arrayOf(NetworkingModule::class))
abstract class InteractorModule {

    @Binds
    abstract fun moviesInteractor(moviesInteractorImpl: MoviesInteractorImpl): MoviesInteractor
}