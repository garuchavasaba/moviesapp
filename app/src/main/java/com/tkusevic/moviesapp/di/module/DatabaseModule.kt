package com.tkusevic.moviesapp.di.module

import com.tkusevic.moviesapp.firebase.database.DatabaseHelper
import com.tkusevic.moviesapp.firebase.database.DatabaseHelperImpl
import dagger.Binds
import dagger.Module


@Module
abstract class DatabaseModule {

    @Binds
    abstract fun firebaseDatabase(databaseHelperImpl: DatabaseHelperImpl): DatabaseHelper
}