package com.tkusevic.moviesapp.di.module

import com.tkusevic.moviesapp.firebase.authentication.AuthenticationHelper
import com.tkusevic.moviesapp.firebase.authentication.AuthenticationHelperImpl
import dagger.Binds
import dagger.Module


@Module(includes = arrayOf(DatabaseModule::class))
abstract class AuthenticationModule {

    @Binds
    abstract fun authenticationHelper(authenticationHelperImpl: AuthenticationHelperImpl): AuthenticationHelper
}