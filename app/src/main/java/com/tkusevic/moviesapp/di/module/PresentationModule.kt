package com.tkusevic.moviesapp.di.module

import com.tkusevic.moviesapp.presentation.*
import dagger.Binds
import dagger.Module


@Module(includes = arrayOf(InteractorModule::class, AuthenticationModule::class))
abstract class PresentationModule {
    @Binds
    abstract fun profilePresenter(profilePresenterImpl: ProfilePresenterImpl): ProfilePresenter

    @Binds
    abstract fun registrationPresenter(registrationPresenterImpl: RegistrationPresenterImpl): RegistrationPresenter

    @Binds
    abstract fun signInPresenter(signInPresenterImpl: SignInPresenterImpl): SignInPresenter

    @Binds
    abstract fun topRatedPresenter(topRatedPresenterImpl: TopRatedPresenterImpl): TopRatedPresenter

    @Binds
    abstract fun newFilmsPresenter(newFilmsPresenterImpl: NewFilmsPresenterImpl): NewFilmsPresenter

    @Binds
    abstract fun favoritesPresenter(favoritesPresenterImpl: FavoritesPresenterImpl): FavoritesPresenter

    @Binds
    abstract fun movieDetailsPresenter(movieDetailsPresenterImpl: MovieDetailsPresenterImpl): MovieDetailsPresenter

    @Binds
    abstract fun editProfilePresenter(editProfilePresenterImpl: EditProfilePresenterImpl): EditProfilePresenter

    @Binds
    abstract fun splashPresenter(splashPresenterImpl: SplashPresenterImpl): SplashPresenter

    @Binds
    abstract fun movieSearchPresenter(movieSearchPresenterImpl: MovieSearchPresenterImpl): MovieSearchPresenter

    @Binds
    abstract fun searchUserPresenter(searchUserPresenterImpl: SearchUserPresenterImpl): SearchUserPresenter

    @Binds
    abstract fun userDetailsPresenter(userDetailsPresenterImpl: UserDetailsPresenterImpl): UserDetailsPresenter
}