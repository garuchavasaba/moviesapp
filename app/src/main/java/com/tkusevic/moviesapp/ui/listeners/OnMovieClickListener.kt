package com.tkusevic.moviesapp.ui.listeners

import com.tkusevic.moviesapp.data.model.Movie


interface OnMovieClickListener {

    fun onMovieClick(movie: Movie)

    fun onLikeClick(movie: Movie)
}