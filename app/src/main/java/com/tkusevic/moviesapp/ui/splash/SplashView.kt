package com.tkusevic.moviesapp.ui.splash


interface SplashView  {

    fun startApp()

    fun startSignIn()
}