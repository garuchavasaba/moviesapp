package com.tkusevic.moviesapp.ui.movie_details

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.squareup.picasso.Picasso
import com.tkusevic.moviesapp.R
import com.tkusevic.moviesapp.commons.constants.IMAGE_BASE_URL
import com.tkusevic.moviesapp.commons.constants.MOVIE_KEY
import com.tkusevic.moviesapp.commons.extensions.onClick
import com.tkusevic.moviesapp.data.model.Movie
import com.tkusevic.moviesapp.movieDetailsPresenter
import com.tkusevic.moviesapp.presentation.MovieDetailsPresenter
import kotlinx.android.synthetic.main.activity_movie_details.*
import java.io.Serializable


class MovieDetailsActivity : AppCompatActivity(), MovieDetailsView {
    override fun setLike(isLiked: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val presenter: MovieDetailsPresenter by lazy { movieDetailsPresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        initPresenter()
        getMoviesInformation()
        initListeners()
    }

    private fun initListeners() {
        backDetails.onClick { finish() }
    }

    private fun initPresenter() = presenter.setBaseview(this)

    private fun getMoviesInformation() {
        val intent = this.intent
        val bundle: Bundle = intent.extras
        val movie: Serializable? = bundle.getSerializable(MOVIE_KEY)
        showData(movie as Movie)
    }

    override fun showData(movie: Movie) {
        Picasso.with(this)
                .load(IMAGE_BASE_URL + movie.imageUrl)
                .resize(180, 180)
                .centerCrop()
                .into(imageMovieDetails)


        titleMovieDetails.text = movie.title
        ratingMovieDetails.text = String.format("რეიტინგი: " + movie.voteAverage)
        numVotesMovieDetails.text = String.format("ხმები: " + movie.voteNumber)
        descriptionMovieDetails.text = movie.description
    }


}