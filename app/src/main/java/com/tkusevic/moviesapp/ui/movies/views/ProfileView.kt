package com.tkusevic.moviesapp.ui.movies.views

import com.tkusevic.moviesapp.data.model.User


interface ProfileView {

    fun setData(user: User)

    fun makeText(s: String)

    fun goToEdit()

    fun signOut()
}