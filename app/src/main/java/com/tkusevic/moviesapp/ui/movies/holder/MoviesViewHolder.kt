package com.tkusevic.moviesapp.ui.movies.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.Picasso
import com.tkusevic.moviesapp.R
import com.tkusevic.moviesapp.commons.constants.IMAGE_BASE_URL
import com.tkusevic.moviesapp.commons.extensions.onClick
import com.tkusevic.moviesapp.data.model.Movie
import com.tkusevic.moviesapp.ui.listeners.OnMovieClickListener
import kotlinx.android.synthetic.main.holder_movies.view.*


class MoviesViewHolder(private val listener: OnMovieClickListener, itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun setMovie(movie: Movie) = with(itemView) {
        Picasso.with(context)
                .load(IMAGE_BASE_URL + movie.imageUrl)
                .resize(720, 720)
                .centerCrop()
                .into(image)
        title.text = movie.title
        date.text = String.format("თარიღი: %s", movie.release)
        rating.text = String.format("რეიტინგი: %s", movie.voteAverage)
        numberVotes.text = String.format("ხმები: %s", movie.voteNumber)
        onClick { listener.onMovieClick(movie) }
        //like.onClick { listener.onLikeClick(movie) }
    }
}