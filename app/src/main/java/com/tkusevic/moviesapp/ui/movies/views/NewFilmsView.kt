package com.tkusevic.moviesapp.ui.movies.views

import com.tkusevic.moviesapp.data.model.Movie


interface NewFilmsView {

    fun setMovies(movies: List<Movie>)

    fun addMovies(movies: List<Movie>)

    fun setFavorites(favorites : List<Movie>)

    fun showMessageEmptyList()

    fun hideMessageEmptyList()
}