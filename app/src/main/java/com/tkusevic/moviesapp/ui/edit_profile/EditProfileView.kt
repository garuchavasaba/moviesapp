package com.tkusevic.moviesapp.ui.edit_profile

import com.tkusevic.moviesapp.data.model.User


interface EditProfileView {

    fun setData(user: User)

    fun editDone()
}