package com.tkusevic.moviesapp.ui.movies.views

import com.tkusevic.moviesapp.data.model.Movie


interface FavoritesView {

    fun setMovies(movies: List<Movie>)

    fun showMessageOnScreen()

    fun hideMessageOnScreen()
}