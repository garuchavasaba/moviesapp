package com.tkusevic.moviesapp.ui.movie_details

import com.tkusevic.moviesapp.data.model.Movie


interface MovieDetailsView {

    fun showData(movie: Movie)

    fun setLike(isLiked: Boolean)
}